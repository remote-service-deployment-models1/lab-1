import sys


def copy_array_from_position(arr1, arr2, start_position):
    # Проверка наличия элементов во втором массиве
    if not arr2:
        return arr1

    # Проверка корректности start_position
    if start_position < 0 or start_position >= len(arr1):
        raise ValueError("Invalid start position")

    # Копирование элементов из arr2 в arr1, начиная с заданной позиции
    for i in range(len(arr2)):
        if start_position + i < len(arr1):
            arr1[start_position + i] = arr2[i]
        else:
            arr1.append(arr2[i])

    return arr1


if __name__ == "__main__":
    array1 = (sys.argv[1]).split(',')
    print("Исходный массив 1:", array1)
    
    array2 = (sys.argv[2]).split(',')
    print("Исходный массив 2:", array2)
    
    position = int(sys.argv[3])
    print("С позиции:", position)
    
    result_array = copy_array_from_position(array1, array2, position)
    print("Результат:", result_array)
